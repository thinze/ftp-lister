<?php


function getFileExtension($fname) {
    $tmp  = explode('.', $fname);
    $ext  = '?';
    if (count($tmp) > 1) {
        $last = array_pop($tmp);
        if (strlen($last) <= 4) {
            $ext = $last;
        }
    }
    return strtolower($ext);
}


function parse_ftp_rawlist($List, $Win = FALSE)
{
  $Output = array();
  $i = 0;
  if ($Win) {
    foreach ($List as $Current) {
      ereg('([0-9]{2})-([0-9]{2})-([0-9]{2}) +([0-9]{2}):([0-9]{2})(AM|PM) +([0-9]+|) +(.+)', $Current, $Split);
      if (is_array($Split)) {
        if ($Split[3] < 70) {
          $Split[3] += 2000;
        }
        else {
          $Split[3] += 1900;
        }
        $Output[$i]['isdir']     = ($Split[7] == '');
        $Output[$i]['size']      = $Split[7];
        $Output[$i]['month']     = $Split[1];
        $Output[$i]['day']       = $Split[2];
        $Output[$i]['time/year'] = $Split[3];
        $Output[$i]['name']      = $Split[8];
        $Output[$i]['ext']       = getFileExtension($Split[8]);
        $i++;
      }
    }
    return !empty($Output) ? $Output : array();   // false;
  }
  else {
    foreach ($List as $Current) {
        if ($Current) {
            $Split = preg_split('[ ]', $Current, 9, PREG_SPLIT_NO_EMPTY);
            //$ if (count($Split) == 1) { var_dump($Current, $Split); echo '<hr>'; var_dump($List); die(); }
            if (count($Split) == 9 && $Split[0] != 'total') {
                $Output[$i]['isdir']     = ($Split[0] {0} === 'd');
                $Output[$i]['perms']     = $Split[0];
                $Output[$i]['number']    = $Split[1];
                $Output[$i]['owner']     = $Split[2];
                $Output[$i]['group']     = $Split[3];
                $Output[$i]['size']      = $Split[4];
                $Output[$i]['month']     = $Split[5];
                $Output[$i]['day']       = $Split[6];
                $Output[$i]['time/year'] = $Split[7];
                $Output[$i]['name']      = $Split[8];
                $Output[$i]['ext']       = getFileExtension($Split[8]);
                $i++;
            }
        }
    }
    return !empty($Output) ? $Output : array();   // FALSE;
  }
}
?>
