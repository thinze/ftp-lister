<?php
/* Utilities f�r die FTP-Liste
*/


define('FOLDER_ACTIONS', '<input type="checkbox" class="collect"><span class="onoff">-</span>');  // &nu;  | &Lambda; 
$skip_paths = array('Media/Audio', 
                    'Media/Schulung', 
                    'Media/Software',
                    'Media/Videos');


function connectToServer($ftp_data) {
    $res      = ftp_connect($ftp_data['svr']);
    $login_ok = ftp_login($res, $ftp_data['usr'], $ftp_data['pwd']);
    
    if (!$res || !$login_ok) {
        echo "Es konnte keine Verbindung mit dem FTP-Server hergestellt werden.";
        die();
    }
    return $res;
}


function getHumanReadableFilesize($fsize) {
    global $cnt;
    
    $fs = $fsize / 1000; // 1024;
    $s  = '';
    switch ($fs) {
        case $fs < 1:
              $s = number_format($fs, 2, '.', '') . ' B'; break;
        case $fs < 1000:
              $s = number_format($fs, 2, '.', '') . ' kB'; break;
        case $fs < 1000000:
              $s = number_format(($fs/1000), 2, '.', '') . ' MB'; break;
        case $fs < 1000000000:
              $s = number_format(($fs/1000000), 2, '.', '') . ' GB'; break;
        case $fs < 1000000000000:
              $s = number_format(($fs/1000000000), 2, '.', '') . ' TB'; break;
    }
    //$ echo $fsize . ' == ' . $fs . ' == ' . $s . '<br />'; $cnt++; if ($cnt==10) { die(); }
    return $s;
}


function read_folders($ftp_data, $lvl=0) {
    /*
      liefert ein Array mit den Infos zu den gefundenen Dateien
    */
    global $res, $skip_paths;
    
    if ($lvl > $ftp_data['max_read_lvl']) { 
        return array(); 
    }

    $file_list = ftp_nlist($res, $ftp_data['start']);
    sort($file_list);
    
    $flist_raw = ftp_rawlist($res, $ftp_data['start']);           //$ var_dump($flist_raw); die();
    $dir_files = parse_ftp_rawlist($flist_raw, false);  //$ var_dump($all_files); die();
    $path_info = array('files' => array(), 'subs' => array());
    foreach ($dir_files as $file_info) {
        switch ($file_info['name']) {
            case 'recycle': 
            case '.wd_tv':
            case in_array($ftp_data['start'] . '/' . $file_info['name'], $skip_paths):
                  continue;
                  break;
            default:
                  break;
        }
        if ($file_info['isdir']) {
            // subfolder found
            $subDir = $ftp_data;
            $subDir['start'] .=  '/' . $file_info['name'];
            $sub_info = read_folders($subDir, $lvl + 1);
            $path_info['subs'][$file_info['name']] = $sub_info;

        } else {
            // add file to current folder list
            $path_info['files'][] = $file_info;
        }
    }
    return $path_info;
}


function getDirInfos($ftp_data, $files_list, $lvl=0) {

    if ($lvl > $ftp_data['max_output_lvl']) { 
        return null; 
    }

    $html = '<ul class="lvl-' . $lvl . '">';
    foreach ($files_list['subs'] as $dir_name => $file_info) {
        if (!in_array($dir_name, array('.', '..', '.wd_tv', 'recycle'))) { 
            $html .= '<li class="folder"><span class="actions">' . FOLDER_ACTIONS . $dir_name . '</span>'; 
            if (count($files_list['subs'])) {
                $html .= getDirInfos($ftp_data, $file_info, $lvl + 1);
            }
            echo '</li>';
        }
    }
    foreach ($files_list['files'] as $file_info) {
        if (!in_array($file_info['name'], array('.', '..', '.wd_tv', 'recycle'))) {
            $fsize = getHumanReadableFilesize($file_info['size']);
            $html .= '<li class="file">'
                    .'<em>' . $file_info['ext'] . '</em>'
                    .'<span>' . $file_info['name'] . '</span>'
                    .'<b>' . $fsize . '</b> </li>';
        }
    }
    $html .= '</ul>';
    return $html;
}


function createCompactList($ftp_data, $structure) {
    return getDirInfos($ftp_data, $structure);
}


?>