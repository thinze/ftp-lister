<?php

function ftp_get_data($directory) {
    global $conn_id;

    $filetypes  = array('-'=>'file', 'd'=>'directory', 'l'=>'link');
    $data       = ftp_rawlist($conn_id,$directory);
    foreach ($data as $line) {
        if (substr(strtolower($line), 0, 5) == 'total') {
            continue;
        } 
        preg_match('/'. str_repeat('([^s]+)s+', 7) .'([^s]+) (.*)/', $line, $matches); 
        list($permissions, $children, $owner, $group, 
             $size, $month, $day, $time, $name)       = array_slice($matches, 1);
        if (!in_array($permissions[0], array_keys($filetypes))) {
            continue;
        }
        $type = $filetypes[$permissions[0]];
        $files[$directory][$name] = array('type'=>$type, 
                                          'permissions'=>substr($permissions, 1), 
                                          'children'=>$children, 
                                          'owner'=>$owner, 
                                          'group'=>$group, 
                                          'size'=>$size);
    }
    return $files;
}

?>
