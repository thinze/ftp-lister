/* Javascript f�r FTP-Liste */

// require jQuery


var types = new Array();
types['ordner'] = null; 
types['bilder'] = ['gif', 'jpg', 'jpeg', 'png', 'tiff', 'psd'];
types['audio']  = ['mp3', 'wav', 'ogg'];
types['video']  = ['avi', 'mpeg', 'mov', 'mp4', 'mkv'];


function toggleFolderOnOff() {
    var ref = $(this);
    var ul  = ref.parent().next('ul');
    if (ul.hasClass('open')) {
        // close folder
        collapseFolder(ul);
    } else {
        // open folder
        expandFolder(ul);
    }
}


function initList() {
    // store the height of any list as data attribute
    $('ul, li.folder').each( function() {
        var ref = $(this);
        ref.attr('data-height', ref.height());
    });
    // init the click event for the onoff-button
    $('li.folder .actions .onoff').each( function() {
        var ref = $(this);
        ref.click( toggleFolderOnOff );
        // mark all folder with no child list als disabled
        if (ref.parent().next('ul').length == 0) {
            ref.text('X').css('color', 'red').click(function() { return false; });
        } else {
            ref.css('cursor', 'pointer');
        }
    });
    $('ul.lvl-0').addClass('open');     // the root dir is always open
    
}


function expandAll() {
    $('span.actions').each( function() {
        var ul = $(this).next('ul'); 
        ul.css('height', ul.attr('data-height') + 'px').addClass('open');
    });
    filterListByType();
}


function expandFolder(ref) {
    if (!ref) {
        expandAll();
    } else {
        var ul = $(ref);
        ul.css('height', ul.attr('data-height') + 'px').addClass('open');
    }
}


function collapseAll() {
    $('span.actions').each( function() {
        var ul = $(this).next('ul');
        ul.css('height', '0').removeClass('open');
    });
}

function collapseFolder(ref) {
    if (!ref) {
        collapseAll();
    } else {
        var ul = $(ref);
        ul.css('height', '0').removeClass('open');
    }
}


function filterListByType() {
    var allVal = $(".ext-filter input:checkbox:checked").map(function(){ return $(this).val(); });
    /* suche in allen offenen ! Ordnern nach den Extensions
    */ 
    
    // alle Extensions zum Filtern ermitteln
    var filterBy = new Array();
    allVal.forEach( function() {
        filterBy.concat(types[this]);
    });
    
    if (allVal.length) {
        if ($.inArray('dir', allVal)) {
            $('.open > li.file').addClass('filter-out');
        } else {
            $('.open > li.file').each( function() {
                var ref = $(this);
                if ($.inArray($('em', ref).text(), filterBy) == -1) {
                    ref.addClass('filter-out');
                } else {
                    ref.removeClass('filter-out');
                }
            });
        }
    } else {
        // enable all
        $('li.filter-out').removeClass('filter-out');
    }
}


function initActions() {
    $('#expand-all').click( expandAll );
    $('#collapse-all').click( collapseAll );
    $('.ext-filter input').click( filterListByType );
}


$(document).ready( function() {
    initList();
    collapseFolder();   // all
    initActions();
});
